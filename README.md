# core-dump-containers

Container Definitions for debugging core dumps using the core-dump-client.
See https://github.com/IBM/core-dump-handler/tree/main/core-dump-client

### CLI Component Layout
![Client Component Diagram](assets/client_topology.png)

## build status

### Default
[![Docker Repository on Quay](https://quay.io/repository/icdh/default/status "Docker Repository on Quay")](https://quay.io/repository/icdh/default)
### Node.Js
[![Docker Repository on Quay](https://quay.io/repository/icdh/nodejs/status "Docker Repository on Quay")](https://quay.io/repository/icdh/nodejs)
### Java
[![Docker Repository on Quay](https://quay.io/repository/icdh/nodejs/status "Docker Repository on Quay")](https://quay.io/repository/icdh/java)